import React, { useEffect } from 'react';
import Cookies from 'js-cookie';
import './app.scss';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { checkSuperAdmin } from './redux/settings/actions';
import { getUserInfo } from './redux/auth/actions';

// compnents
import Loader from './components/Loader';
import SuperAdminSignUp from './components/SuperAdminSignUp';
import Routes from './Routes';

function App() {
  const isLoading = useSelector(state => state.settings.isLoading);
  const isRequesting = useSelector(state => state.auth.isRequesting);
  const isInit = useSelector(state => state.settings.admin);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('init app');

    // check for active cookie session
    const token = Cookies.get('session');
    // get current logged user data from server and save to state
    if (token) dispatch(getUserInfo(token));

    // check if super admin exists
    dispatch(checkSuperAdmin());
  }, [dispatch]);

  const components = () => {
    if (isLoading || isRequesting) {
      return <Loader />;
    } else {
      if (isInit) return <Routes />;
      return <SuperAdminSignUp />;
    }
  };

  return <div className="App">{components()}</div>;
}

export default App;
