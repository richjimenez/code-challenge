import React, { useState } from 'react';
import { Form, Select, Input, Button, Typography, notification } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { queryUsers } from '../redux/users/actions';
import { signUpUser, signUpStatus } from '../redux/auth/actions';

const { Title } = Typography;
const { Option } = Select;

const SignUp = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const isRequestingUsers = useSelector(state => state.users.isRequesting);
  const clinicians = useSelector(state => state.users.data);
  const reqStatus = useSelector(state => state.auth.signUpStatus);
  const [role, setRole] = useState(null);

  const handleOnChangeRole = role => {
    setRole(role);
    // get clinicians from db
    if (role === 'patient') dispatch(queryUsers({ role: 'clinician' }));
  };

  const onFinish = values => {
    // validate password
    if (values.password !== values.password_confirm) {
      notification.warning({
        message: `Passwords do not match`,
        description: 'Your password and confirmation password do not match.',
      });
    } else {
      if (values.role === 'patient') {
        const arr = values.clinician.split('|');
        values.clinician = { id: arr[0], email: arr[1] };
      }
      delete values.password_confirm;
      dispatch(signUpUser(values));
    }
  };

  // success redirect
  if (reqStatus === 'success')
    setTimeout(() => {
      history.push('/');
      //reset req status
      dispatch(signUpStatus(null));
    }, 500);

  return (
    <div className="animated fadeIn sign-in">
      <Form name="normal_login" className="register-super-admin-form" onFinish={onFinish}>
        <Title>Sign Up</Title>
        <Form.Item name="firstName" rules={[{ required: true, message: 'Please input your first name' }]}>
          <Input prefix={<UserOutlined />} placeholder="First Name" />
        </Form.Item>
        <Form.Item name="lastName" rules={[{ required: true, message: 'Please input your last name' }]}>
          <Input prefix={<UserOutlined />} placeholder="Last name" />
        </Form.Item>
        <Form.Item name="email" rules={[{ required: true, pattern: /\S+@\S+\.\S+/, message: 'Invalid Email' }]}>
          <Input prefix={<MailOutlined />} placeholder="Email" type="email" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, min: 8 }]}>
          <Input prefix={<LockOutlined />} type="password" placeholder="Password" />
        </Form.Item>
        <Form.Item name="password_confirm" rules={[{ required: true, min: 8 }]}>
          <Input prefix={<LockOutlined />} type="password" placeholder="Confirm Password" />
        </Form.Item>

        <Form.Item name="role" rules={[{ required: true }]}>
          <Select placeholder="You are?" onChange={handleOnChangeRole} allowClear>
            <Option value="patient">Patient</Option>
            <Option value="clinician">Clinician</Option>
          </Select>
        </Form.Item>

        {role === 'patient' && (
          <Form.Item name="clinician" rules={[{ required: true }]}>
            <Select placeholder="Select one clinician" allowClear>
              {clinicians.map(c => (
                <Option key={c._id} value={`${c._id}|${c.email}`}>{`${c.firstName} ${c.lastName}`}</Option>
              ))}
            </Select>
          </Form.Item>
        )}

        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={isRequestingUsers}>
            Sign Up
          </Button>
          Or <Link to="/signin">Sign In</Link>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SignUp;
