import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Modal, Typography, Button } from 'antd';

// calendar
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

// redux
import { useSelector, useDispatch } from 'react-redux';
import { setEvent, saveEvents, removeEventByDate, getPatientEvents } from '../redux/events/actions';

const { Title, Paragraph, Text } = Typography;

export default () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);
  const events = useSelector(state => state.events.data);
  const isRequesting = useSelector(state => state.events.isRequesting);
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [selectedDate, setSelectedDate] = useState(null);

  // map events
  const eventsFiltered = () => {
    // get patient events
    const patientEvents = events
      .filter(e => e.patient.id === currentUser._id)
      .map(e => {
        switch (e.status) {
          case 'Pending Approval':
            return { id: e._id, title: e.status, start: e.start, classNames: 'event-pending' };
          case 'Confirmed':
            return { id: e._id, title: e.status, start: e.start, classNames: 'event-confirmed' };
          case 'Declined':
            return { id: e._id, title: e.status, start: e.start, classNames: 'event-declined' };
          default:
            return { id: e._id, title: e.status, start: e.start, classNames: 'event-draft' };
        }
      });

    // get other events
    const otherEvents = events
      .filter(e => e.patient.id !== currentUser._id && e.status !== 'Declined')
      .map(e => ({ id: e._id, title: 'Unavailable', start: e.start, classNames: 'event-not-available' }));

    return patientEvents.concat(otherEvents);
  };
  const draftEvents = events.filter(e => e.status === 'Draft');

  useEffect(() => {
    // get patient events
    dispatch(getPatientEvents(currentUser.clinician.id));
  }, [dispatch, currentUser]);

  const handleDateClick = ({ date }) => {
    setShowAddModal(true);
    setSelectedDate(date);
  };

  const handleEventClick = arg => {
    // get selected date from calendar
    if (arg.event.title === 'Draft') {
      setShowEditModal(true);
      setSelectedDate(arg.event.start);
    }
  };

  const deleteEventDraft = () => {
    // remove draft events by date
    dispatch(removeEventByDate(selectedDate));
    setShowEditModal(false);
  };

  const saveEventDraft = () => {
    // save draft events to storage to keep even if close the browser????
    const event = {
      _id: currentUser._id,
      title: 'Draft',
      status: 'Draft',
      start: selectedDate,
      classNames: 'event-draft',
      patient: { id: currentUser._id, email: currentUser.email },
      clinician: currentUser.clinician,
    };

    dispatch(setEvent(event));
    setShowAddModal(false);
  };

  const sendEvents = () => {
    // get draft events from state
    const data = events
      .filter(e => e.status === 'Draft')
      .map(e => ({
        status: 'Pending Approval',
        start: e.start,
        patient: e.patient,
        clinician: e.clinician,
      }));
    dispatch(saveEvents(data));
  };

  const handleCancel = () => {
    setShowAddModal(false);
    setShowEditModal(false);
  };

  return (
    <div>
      <Title level={3}>Your clinician calendar</Title>
      <p>Please click on available dates to select your appointments, once you have choose your dates click on "Reserve Appointments"</p>
      <FullCalendar
        timeZone={'local'}
        defaultView="timeGridWeek"
        allDaySlot={false}
        minTime={'08:00:00'}
        maxTime={'21:00:00'}
        slotDuration={'01:00:00'}
        contentHeight={'auto'}
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        timeGridEventMinHeight={40}
        dateClick={handleDateClick}
        eventClick={handleEventClick}
        events={eventsFiltered()}
      />
      <Button
        type="primary"
        size="large"
        style={{ marginTop: 15 }}
        onClick={sendEvents}
        disabled={draftEvents.length === 0}
        loading={isRequesting}
        block
      >
        Reserve Appointments
      </Button>
      {/* Add new event modal */}
      <Modal title="Add new appointment draft" okText={'Yes, create it'} visible={showAddModal} onOk={saveEventDraft} onCancel={handleCancel}>
        <Paragraph>
          You're about to create an appointment draft for <Text mark>{moment(selectedDate).format('MMMM Do YYYY, h:mm A')}</Text> Are you sure?
        </Paragraph>
      </Modal>
      {/* Edit event modal */}
      <Modal
        title="Edit appointment draft"
        okText={'Yes, delete it'}
        okType={'danger'}
        visible={showEditModal}
        onOk={deleteEventDraft}
        onCancel={handleCancel}
      >
        <Paragraph>
          You're about to delete an appointment draft for <Text mark>{moment(selectedDate).format('MMMM Do YYYY, h:mm A')}</Text> Are you sure?
        </Paragraph>
      </Modal>
    </div>
  );
};
