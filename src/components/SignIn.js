import React from 'react';
import { Form, Input, Button, Typography } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Redirect, Link } from 'react-router-dom';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { signInUser } from '../redux/auth/actions';

const { Title } = Typography;

export default () => {
  const dispatch = useDispatch();
  const isRequesting = useSelector((state) => state.auth.isRequesting);
  const currentUser = useSelector((state) => state.auth.currentUser);

  const onFinish = (values) => {
    dispatch(signInUser(values));
  };

  if (!currentUser) {
    return (
      <div className="animated fadeIn sign-in">
        <Form name="normal_login" className="sign-up-form" onFinish={onFinish}>
          <Title>Sign In</Title>
          <Form.Item name="email" rules={[{ required: true, pattern: /\S+@\S+\.\S+/, message: 'Please input your email' }]}>
            <Input prefix={<UserOutlined />} type="email" placeholder="Email" />
          </Form.Item>
          <Form.Item name="password" rules={[{ required: true, message: 'Please input a valid password', min: 8 }]}>
            <Input prefix={<LockOutlined />} type="password" placeholder="Password" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" block loading={isRequesting}>
              Sign In
            </Button>
            Or <Link to="/signup">Sign Up</Link>
          </Form.Item>
        </Form>
      </div>
    );
  } else {
    return <Redirect to="/" />;
  }
};
