import React from 'react';
import { Form, Input, Button, Typography, notification } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
// redux
import { useDispatch } from 'react-redux';
import { registerSuperAdmin } from '../redux/settings/actions';

const { Title } = Typography;
const emailrex = /\S+@\S+\.\S+/;

const SuperAdminSignUp = () => {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    // validate password
    if (values.password !== values.password_confirm) {
      notification.warning({
        message: `Passwords do not match`,
        description: 'Your password and confirmation password do not match.',
      });
    } else {
      delete values.password_confirm;
      dispatch(registerSuperAdmin(values));
    }
  };

  return (
    <div>
      <div className="sign-in">
        <Form name="normal_login" className="register-super-admin-form" onFinish={onFinish}>
          <Title>Register Admin</Title>
          <Form.Item name="firstName" rules={[{ required: true, message: 'Please input your first name' }]}>
            <Input prefix={<UserOutlined />} placeholder="First Name" />
          </Form.Item>
          <Form.Item name="lastName" rules={[{ required: true, message: 'Please input your last name' }]}>
            <Input prefix={<UserOutlined />} placeholder="Last name" />
          </Form.Item>
          <Form.Item name="email" rules={[{ required: true, pattern: emailrex, message: 'Invalid Email' }]}>
            <Input prefix={<MailOutlined />} placeholder="Email" type="email" />
          </Form.Item>
          <Form.Item name="password" rules={[{ required: true, min: 8 }]}>
            <Input prefix={<LockOutlined />} type="password" placeholder="Password" />
          </Form.Item>
          <Form.Item name="password_confirm" rules={[{ required: true, min: 8 }]}>
            <Input prefix={<LockOutlined />} type="password" placeholder="Confirm Password" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Register
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default SuperAdminSignUp;
