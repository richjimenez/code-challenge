import React from 'react';
import { Layout, Menu } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Cookies from 'js-cookie';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentUser } from '../redux/auth/actions';

//components
import SuperAdminDashboard from './SuperAdminDashboard';
import ClinicianDashboard from './ClinicianDashboard';
import PatientDashboard from './PatientDashboard';

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;

const Dashboard = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);

  // get token info
  const token = Cookies.get('session').split('.');
  const tokenPayload = JSON.parse(atob(token[1]));

  // render dashboards
  const dashboard = () => {
    switch (tokenPayload.role) {
      case 'sadmin':
        return <SuperAdminDashboard />;
      case 'clinician':
        return <ClinicianDashboard />;
      case 'patient':
        return <PatientDashboard />;
      default:
        return <h1>NEIN</h1>;
    }
  };

  const handleMenuClick = item => {
    console.log(item);
    switch (item.key) {
      case 'sign-out':
        // remove cookie and update the state
        Cookies.remove('session');
        dispatch(setCurrentUser(null));
        // TODO destroy token in server to?
        break;
      default:
        break;
    }
  };

  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" onClick={handleMenuClick}>
          <SubMenu
            title={
              <span>
                <UserOutlined />
                {currentUser.firstName + ' ' + currentUser.lastName}
              </span>
            }
            style={{ float: 'right' }}
          >
            <Menu.Item key="sign-out">Sign out</Menu.Item>
          </SubMenu>
        </Menu>
      </Header>
      <Content style={{ padding: '0 50px', marginTop: 20 }}>
        <div className="site-layout-content">{dashboard()}</div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Rick ©2020</Footer>
    </Layout>
  );
};

export default Dashboard;
