import React, { useEffect, useState } from 'react';
import { Table, Typography, Button, Modal, Descriptions, Select } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import moment from 'moment';

// Redux
import { useDispatch, useSelector } from 'react-redux';
import { queryUsers, updateUser } from '../redux/users/actions';

const { Title } = Typography;
const { Option } = Select;

export default () => {
  const dispatch = useDispatch();
  const [editStatus, setEditStatus] = useState(false);
  const [currentClinician, setCurrentClinician] = useState({});
  const isRequesting = useSelector(state => state.users.isRequesting);
  const columns = [
    {
      title: 'First Name',
      dataIndex: 'firstName',
      sorter: (a, b) => a.firstName.localeCompare(b.firstName),
    },
    {
      title: 'Last Name',
      dataIndex: 'lastName',
      sorter: (a, b) => a.lastName.localeCompare(b.lastName),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      filters: [
        {
          text: 'Verified',
          value: 'Verified',
        },
        {
          text: 'Unverified',
          value: 'Unverified',
        },
        {
          text: 'Disabled',
          value: 'Disabled',
        },
      ],
      // specify the condition of filtering result
      // here is that finding the name started with `value`
      render: (val, obj) => (
        <div>
          <Button
            shape="circle"
            icon={<EditOutlined />}
            size={'small'}
            onClick={() => {
              setEditStatus(true);
              setCurrentClinician(obj);
            }}
          />{' '}
          {val}
        </div>
      ),
      onFilter: (value, record) => record.status.indexOf(value) === 0,
      sorter: (a, b) => a.status.localeCompare(b.status),
    },
    {
      title: 'Email',
      dataIndex: 'email',
      sorter: (a, b) => a.email.localeCompare(b.email),
    },
    {
      title: 'Sign up date',
      dataIndex: 'createdAt',
      render: val => moment(val).format('MMMM Do YYYY, h:mm A'),
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
    },
  ];
  const users = useSelector(state => state.users.data);
  // add key field
  const clinicians = users.map(user => ({ ...user, key: user._id }));

  useEffect(() => {
    // get clinicians
    dispatch(queryUsers({ role: 'clinician' }));
  }, [dispatch]);

  const onChangeStatus = value => {
    setCurrentClinician(prev => ({ ...prev, status: value }));
  };

  const updateStatus = () => {
    dispatch(updateUser(currentClinician));
    setEditStatus(false);
  };

  return (
    <div>
      <Title>Clinicians</Title>
      <Table columns={columns} dataSource={clinicians} />

      <Modal
        visible={editStatus}
        title=""
        okText="Update"
        cancelText="Cancel"
        onCancel={() => setEditStatus(false)}
        onOk={updateStatus}
        confirmLoading={isRequesting}
      >
        <Descriptions title="Clinician Info:" layout="vertical" bordered style={{ marginBottom: 20 }}>
          <Descriptions.Item label="Name">
            {currentClinician.firstName} {currentClinician.lastName}
          </Descriptions.Item>
          <Descriptions.Item label="Email">{currentClinician.email}</Descriptions.Item>
          <Descriptions.Item label="Status">{currentClinician.status}</Descriptions.Item>
          <Descriptions.Item label="Sign Up date">{moment(currentClinician.createdAt).format('MMMM Do YYYY, h:mm A')}</Descriptions.Item>
        </Descriptions>

        <span>Select new status:</span>
        <Select placeholder="Select a status" onChange={onChangeStatus} value={currentClinician.status} style={{ width: '100%' }}>
          <Option value="Verified">Verified</Option>
          <Option value="Unverified">Unverified</Option>
          <Option value="Disabled">Disabled</Option>
        </Select>
      </Modal>
    </div>
  );
};
