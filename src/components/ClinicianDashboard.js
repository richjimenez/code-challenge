import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Descriptions, Modal, Select, Tag, Typography } from 'antd';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
//redux
import { getPatientEvents, updateEvent } from '../redux/events/actions';
import { useDispatch, useSelector } from 'react-redux';

const { Title } = Typography;
const { Option } = Select;

const ClinicianDashboard = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);
  const events = useSelector(state => state.events.data);
  // const isRequesting = useSelector(state => state.events.isRequesting);
  const [showEditEventModal, setShowEditEventModal] = useState(false);
  const [currentEvent, setCurrentEvent] = useState(null);
  const [currentEventStatus, setCurrentEventStatus] = useState(null);

  const formattedEvents = events.map(e => {
    switch (e.status) {
      case 'Pending Approval':
        return { id: e._id, title: e.patient.email, start: e.start, classNames: 'event-pending' };
      case 'Confirmed':
        return { id: e._id, title: e.patient.email, start: e.start, classNames: 'event-confirmed' };
      case 'Declined':
        return { id: e._id, title: e.patient.email, start: e.start, classNames: 'event-declined' };
      default:
        return { id: e._id, title: e.patient.email, start: e.start, classNames: 'event-draft' };
    }
  });

  useEffect(() => {
    // get patient events
    dispatch(getPatientEvents(currentUser._id));
  }, [dispatch, currentUser]);

  const handleEventClick = arg => {
    const [event] = events.filter(e => e._id === arg.event.id);
    setCurrentEvent(event);
    setCurrentEventStatus(event.status);
    setShowEditEventModal(true);
  };

  const handleOnOK = () => {
    const appointment = { ...currentEvent, status: currentEventStatus };
    if (currentEvent.status !== currentEventStatus) dispatch(updateEvent(appointment));
    setShowEditEventModal(false);
  };

  const handleCancel = () => {
    setShowEditEventModal(false);
  };

  return (
    <div className="animated fadeIn">
      <Title level={3}>Your calendar</Title>
      <p>Please click on available dates to select your appointments, once you have choose your dates click on "Save and send for approval"</p>
      <p>
        <span>Color guide: </span>
        <Tag color="success">Confirmed</Tag>
        <Tag color="processing">Pending Approval</Tag>
        <Tag color="error">Declined</Tag>
        {/*<Tag color="default">Not available</Tag>*/}
        {/*<Tag color="warning">Draft</Tag>*/}
      </p>
      <FullCalendar
        timeZone={'local'}
        defaultView="timeGridWeek"
        theme={'materia'}
        allDaySlot={false}
        minTime={'08:00:00'}
        maxTime={'21:00:00'}
        slotDuration={'01:00:00'}
        contentHeight={'auto'}
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        timeGridEventMinHeight={40}
        eventClick={handleEventClick}
        events={formattedEvents}
      />
      {/* Edit event modal */}
      <Modal title="Edit appointment draft" okText={'Update Status'} visible={showEditEventModal} onOk={handleOnOK} onCancel={handleCancel}>
        {currentEvent && (
          <div>
            <Descriptions title="Clinician Info:" layout="vertical" bordered style={{ marginBottom: 20 }}>
              <Descriptions.Item label="Patient">{currentEvent.patient.email}</Descriptions.Item>
              <Descriptions.Item label="Date">{moment(currentEvent.start).format('MMMM Do YYYY, h:mm A')}</Descriptions.Item>
              <Descriptions.Item label="Status">{currentEvent.status}</Descriptions.Item>
            </Descriptions>

            <span>Select new status:</span>
            <Select placeholder="Select a status" onChange={s => setCurrentEventStatus(s)} value={currentEventStatus} style={{ width: '100%' }}>
              <Option value="Confirmed">Confirmed</Option>
              <Option value="Pending Approval">Pending Approval</Option>
              <Option value="Declined">Declined</Option>
            </Select>
          </div>
        )}
      </Modal>
    </div>
  );
};

export default ClinicianDashboard;
