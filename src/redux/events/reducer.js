import types from './types';
import moment from 'moment';

const initState = { data: [], isRequesting: false };

const eventsReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.SET_EVENTS:
      return {
        ...state,
        data: payload,
      };
    case types.SET_EVENT:
      return {
        ...state,
        data: state.data.concat(payload),
      };

    case types.REMOVE_EVENT_BY_DATE:
      return {
        ...state,
        data: state.data.filter(event => !moment(event.start).isSame(payload)),
      };

    case types.DELETE_EVENT:
      return {
        ...state,
        data: state.data.filter(event => event._id !== payload._id),
      };

    case types.SET_UPDATED_EVENT:
      return {
        ...state,
        data: state.data.map(event => (event._id === payload._id ? payload : event)),
      };

    case types.IS_REQUESTING_EVENTS:
      return {
        ...state,
        isRequesting: payload,
      };

    default:
      return state;
  }
};

export default eventsReducer;
