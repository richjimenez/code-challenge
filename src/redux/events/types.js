export default {
  GET_PATIENT_EVENTS: 'GET_PATIENT_EVENTS',
  SAVE_EVENTS: 'SAVE_EVENTS',
  SET_EVENTS: 'SET_EVENTS',
  SET_EVENT: 'SET_EVENT',
  UPDATE_EVENT: 'UPDATE_EVENT',
  SET_UPDATED_EVENT: 'SET_UPDATED_EVENT',
  DELETE_EVENT: 'DELETE_EVENT',
  REMOVE_EVENT_BY_DATE: 'REMOVE_EVENT_BY_DATE',
  IS_REQUESTING_EVENTS: 'IS_REQUESTING_EVENTS',
};
