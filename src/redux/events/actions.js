import types from './types';

export const setEvents = payload => ({ type: types.SET_EVENTS, payload });
export const setEvent = payload => ({ type: types.SET_EVENT, payload });
export const updateEvent = payload => ({ type: types.UPDATE_EVENT, payload });
export const setUpdatedEvent = payload => ({ type: types.SET_UPDATED_EVENT, payload });
export const getPatientEvents = payload => ({ type: types.GET_PATIENT_EVENTS, payload });
export const saveEvents = payload => ({ type: types.SAVE_EVENTS, payload });
export const deleteEvent = payload => ({ type: types.DELETE_EVENT, payload });
export const removeEventByDate = payload => ({ type: types.REMOVE_EVENT_BY_DATE, payload });
export const setIsRequesting = payload => ({ type: types.IS_REQUESTING_EVENTS, payload });
