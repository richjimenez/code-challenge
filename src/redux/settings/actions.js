import types from './types';

export const checkSuperAdmin = (payload) => ({ type: types.CHECK_SUPER_ADMIN, payload });
export const registerSuperAdmin = (payload) => ({ type: types.REGISTER_SUPER_ADMIN, payload });
export const setIsLoading = (payload) => ({ type: types.SET_IS_LOADING, payload });
