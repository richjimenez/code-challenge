import types from './types';
const initState = { isLoading: true, admin: null };

const settingsReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.SAVE_SUPER_ADMIN:
      return {
        ...state,
        admin: payload,
      };

    case types.SET_IS_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    default:
      return state;
  }
};

export default settingsReducer;
