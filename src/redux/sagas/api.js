import axios from 'axios';
import Cookies from 'js-cookie';

export default (method, endpoint, data) => {
  const token = Cookies.get('session');
  let url = process.env.REACT_APP_API_URL;
  const headers = { Authorization: `Bearer ${token}` };
  url += endpoint;
  if (method === 'get') return axios({ method, url, params: data, headers });
  return axios({ method, url, data, headers });
};
