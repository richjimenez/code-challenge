import { all } from 'redux-saga/effects';
import auth from './auth.js';
import settings from './settings.js';
import users from './users.js';
import events from './events';

export default function* rootSaga() {
  yield all([auth(), settings(), users(), events()]);
}
