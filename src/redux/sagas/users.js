import { put, call, takeLatest } from 'redux-saga/effects';
import { notification, message } from 'antd';
import types from '../users/types';
import api from './api';
import Cookies from 'js-cookie';

const isRequesting = payload => put({ type: types.IS_REQUESTING_USERS, payload });
// const delay = ms => new Promise(res => setTimeout(res, ms));

function* getUsersByQuery({ payload }) {
  yield isRequesting(true);

  try {
    const token = Cookies.get('session');
    const { data } = yield call(api, 'get', '/users/query', payload, token);
    yield isRequesting(false); // disable loading
    if (data.length === 0) {
      return notification.warn({
        message: 'No clinicians found',
        description: 'Please register a clinician first',
      });
    }
    yield put({ type: types.SET_USERS, payload: data });
  } catch (err) {
    console.log(err.response);
    yield isRequesting(false);
    notification.error({
      message: 'Error',
      description: err.response.data.message,
    });
  }
}

function* updateUser({ payload }) {
  const hide = message.loading('Please wait...', 0);
  yield isRequesting(true);
  try {
    const token = Cookies.get('session');
    const endpoint = `/users/updateClinician/${payload._id}`;
    const { data } = yield call(api, 'put', endpoint, payload, token);
    console.log(data);
    yield put({ type: types.SET_UPDATE_USER, payload });
    yield isRequesting(false);
    hide();
  } catch (err) {
    console.log(err.response);
    yield put({ type: types.IS_REQUESTING, payload: false });
    hide();
    notification.error({
      message: 'Error',
      description: err.response.data.message,
    });
  }
}

// watchers
export default function* users() {
  yield takeLatest(types.QUERY_USERS, getUsersByQuery);
  yield takeLatest(types.UPDATE_USER, updateUser);
}
