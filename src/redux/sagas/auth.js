import { put, call, takeLatest } from 'redux-saga/effects';
import { notification } from 'antd';
import Cookies from 'js-cookie';
import types from '../auth/types';
import api from './api';

const isRequesting = payload => put({ type: types.IS_REQUESTING_AUTH, payload });

function* signInUser({ payload }) {
  isRequesting(true);
  try {
    const { data } = yield call(api, 'post', '/auth/signin', payload);
    const { token } = data;
    const { user } = data;
    // get user id from token
    const jwt = token.split('.');
    const tokenPayload = JSON.parse(atob(jwt[1]));
    // save token in a cookie and set de expire time from token
    const expires = new Date(0);
    expires.setUTCSeconds(tokenPayload.exp);
    Cookies.set('session', token, { expires });
    // save info to state
    yield put({ type: types.SET_CURRENT_USER, payload: user });
    yield isRequesting(false);
  } catch (err) {
    yield isRequesting(false);

    if (err.response) {
      notification.error({
        message: 'Error',
        description: err.response.data.message,
      });
    } else {
      notification.error({
        message: 'Error',
        description: 'Something wrong.',
      });
    }
  }
}

function* signUpUser({ payload }) {
  yield isRequesting(true);
  try {
    const { data } = yield call(api, 'post', '/auth/signup', payload);
    console.log(data);
    yield put({ type: types.SET_SIGN_UP_STATUS, payload: 'success' });
    yield isRequesting(false);
    notification.success({
      message: 'Your user has been created',
      description: 'Now you can sign in with your password',
    });
  } catch (err) {
    console.log(err.response);
    yield put({ type: types.SET_SIGN_UP_STATUS, payload: 'failure' });
    yield isRequesting(false);
    notification.error({
      message: 'Error',
      description: err.response.data.message,
    });
  }
}

function* getUserInfo({ payload }) {
  yield isRequesting(true);
  try {
    // get user id from token
    const jwt = payload.split('.');
    const tokenPayload = JSON.parse(atob(jwt[1]));
    // get user info
    const { data } = yield call(api, 'get', '/users/' + tokenPayload.id);
    // save info too state
    yield put({ type: types.SET_CURRENT_USER, payload: data });
    yield isRequesting(false);
  } catch (err) {
    yield isRequesting(false);
    if (err.response) {
      console.log(err.response);
      notification.error({
        message: 'Error',
        description: err.response.data.message,
      });
    } else {
      notification.error({
        message: 'Error',
        description: 'Something wrong',
      });
    }
  }
}

// watchers
export default function* auth() {
  yield takeLatest(types.SIGN_IN_USER, signInUser);
  yield takeLatest(types.SIGN_UP_USER, signUpUser);
  yield takeLatest(types.GET_USER_INFO, getUserInfo);
}
