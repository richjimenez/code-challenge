import { put, call, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import types from '../events/types';
import api from './api';

const isRequesting = payload => put({ type: types.IS_REQUESTING_EVENTS, payload });

function* saveEvents({ payload }) {
  yield isRequesting(true);
  try {
    const { data } = yield call(api, 'post', '/events/multiple', payload);
    // save events to state
    for (const event of data) {
      yield put({ type: types.REMOVE_EVENT_BY_DATE, payload: event.start }); // this remove drafts from state
      yield put({ type: types.SET_EVENT, payload: event });
    }
    yield isRequesting(false);
  } catch (err) {
    console.log(err.response);
    yield isRequesting(false);
    message.warning(err.response.data.message);
  }
}

function* getPatientEvents({ payload }) {
  try {
    yield isRequesting(true);
    const params = { 'clinician.id': payload };
    const { data } = yield call(api, 'get', '/events', params);
    yield put({ type: types.SET_EVENTS, payload: data });
    yield isRequesting(false);
  } catch (err) {
    console.log(err.response);
    yield isRequesting(false);
    message.warning(err.response.data.message);
  }
}

function* updateEvent({ payload }) {
  try {
    yield isRequesting(true);
    const { _id } = payload;
    delete payload._id;
    const { data } = yield call(api, 'put', '/events/' + _id, payload);
    delete data.__v;
    yield put({ type: types.SET_UPDATED_EVENT, payload: data });
    yield isRequesting(false);
  } catch (err) {
    console.log(err.response);
    yield isRequesting(false);
    message.warning(err.response.data.message);
  }
}

// watchers
export default function* settings() {
  yield takeLatest(types.SAVE_EVENTS, saveEvents);
  yield takeLatest(types.UPDATE_EVENT, updateEvent);
  yield takeLatest(types.GET_PATIENT_EVENTS, getPatientEvents);
}
