import { put, call, takeLatest } from 'redux-saga/effects';
import { message, notification } from 'antd';
import Cookies from 'js-cookie';
import types from '../settings/types';
import api from './api';

function* checkSuperAdmin() {
  try {
    // tmp
    Cookies.remove('init');
    const init = Cookies.get('init');
    if (init) {
      // get super admin data from cookie (to reduce api calls every time user open the app)
      yield put({ type: types.SAVE_SUPER_ADMIN, payload: JSON.parse(init) });
      yield put({ type: types.SET_IS_LOADING, payload: false });
    } else {
      // check if super admin exists
      console.log('consulta al api');
      const { data } = yield call(api, 'get', '/users/init');
      Cookies.set('init', JSON.stringify(data), { expires: 365 });
      yield put({ type: types.SAVE_SUPER_ADMIN, payload: data });
      yield put({ type: types.SET_IS_LOADING, payload: false });
    }
  } catch (err) {
    console.log(err);
    yield put({ type: types.SET_IS_LOADING, payload: false });
    if (err.response) {
      console.log(err.response);
      notification.error({
        message: 'Error',
        description: err.response.data.message,
      });
    } else {
      notification.error({
        message: 'Error',
        description: 'Something wrong',
      });
    }
  }
}

function* registerSuperAdmin({ payload }) {
  try {
    const response = yield call(api, 'post', '/users/register-super-admin', payload);
    // save super admin
    Cookies.set('init', response.data, { expires: 365 });
    yield put({ type: types.SAVE_SUPER_ADMIN, payload: response.data });
    message.success('Admin has been registered.');
  } catch (err) {
    console.log(err.response);
    message.warning(err.response.data.message);
  }
}

// watchers
export default function* settings() {
  yield takeLatest(types.REGISTER_SUPER_ADMIN, registerSuperAdmin);
  yield takeLatest(types.CHECK_SUPER_ADMIN, checkSuperAdmin);
}
