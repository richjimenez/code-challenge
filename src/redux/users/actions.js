import types from './types';

export const setUsers = payload => ({ type: types.SET_USERS, payload });
export const queryUsers = payload => ({ type: types.QUERY_USERS, payload });
export const updateUser = payload => ({ type: types.UPDATE_USER, payload });
export const setUpdateUser = payload => ({ type: types.SET_UPDATE_USER, payload });
export const setIsRequesting = payload => ({ type: types.IS_REQUESTING_USERS, payload });
