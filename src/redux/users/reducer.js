import types from './types';
const initState = { data: [], isRequesting: false };

const usersReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.SET_USERS:
      return {
        ...state,
        data: payload,
      };

    case types.SET_UPDATE_USER:
      return {
        ...state,
        data: state.data.map(user => (user._id === payload._id ? payload : user)),
      };

    case types.IS_REQUESTING_USERS:
      return {
        ...state,
        isRequesting: payload,
      };

    default:
      return state;
  }
};

export default usersReducer;
