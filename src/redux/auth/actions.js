import types from './types';

export const getUserInfo = payload => ({ type: types.GET_USER_INFO, payload });
export const setCurrentUser = payload => ({ type: types.SET_CURRENT_USER, payload });
export const signInUser = payload => ({ type: types.SIGN_IN_USER, payload });
export const signUpUser = payload => ({ type: types.SIGN_UP_USER, payload });
export const signUpStatus = payload => ({ type: types.SET_SIGN_UP_STATUS, payload });
export const setIsRequesting = payload => ({ type: types.IS_REQUESTING_AUTH, payload });
