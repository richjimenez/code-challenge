import types from './types';
const initState = { currentUser: null, signUpStatus: null, isRequesting: false };

const authReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case types.SET_CURRENT_USER:
      return {
        ...state,
        currentUser: payload,
      };

    case types.SET_SIGN_UP_STATUS:
      return {
        ...state,
        signUpStatus: payload,
      };

    case types.IS_REQUESTING_AUTH:
      return {
        ...state,
        isRequesting: payload,
      };

    default:
      return state;
  }
};

export default authReducer;
