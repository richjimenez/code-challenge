import { combineReducers } from 'redux';
import authReducer from './auth/reducer';
import settingsReducer from './settings/reducer';
import usersReducer from './users/reducer';
import eventsReducer from './events/reducer';

const reducers = combineReducers({
  auth: authReducer,
  settings: settingsReducer,
  users: usersReducer,
  events: eventsReducer,
});

export default reducers;
